<?php

namespace App\DataFixtures;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class QuestionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Subject $physics */
        $physics = $this->getReference('Subject/Physics');

        /** @var Subject $chemistry */
        $chemistry = $this->getReference('Subject/Chemistry');

        $this->loadPhysicsQuestions($manager, $physics);
        $this->loadChemistryQuestions($manager, $chemistry);

        $manager->flush();
    }

    private function loadPhysicsQuestions(ObjectManager $manager, Subject $physics)
    {
        $question = (new Question())
            ->setQuestionText('Light year is a unit of')
            ->setSubject($physics)
        ;
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Time')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Distance')
                ->setCorrect(true)
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Light')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Intensity of Light')
        );
        $manager->persist($question);

        $question = (new Question())
            ->setQuestionText('Light can be focused on our retina through which of the following phenomena?')
            ->setSubject($physics)
        ;
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Interference')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Refraction')
                ->setCorrect(true)
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Diffraction')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Mirage')
        );
        $manager->persist($question);
    }

    private function loadChemistryQuestions(ObjectManager $manager, Subject $chemistry)
    {
        $question = (new Question())
            ->setQuestionText('Which of the following is not an acid’s property?')
            ->setSubject($chemistry)
        ;
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Acids are sour in taste.')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Acids turn blue litmus red.')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Acids don\'t react with active metals to give hydrogen')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('Acids show a neutralization reaction with bases.')
        );
        $manager->persist($question);

        $question = (new Question())
            ->setQuestionText('The PH of which of the following is more than 7?')
            ->setSubject($chemistry)
        ;
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('gastric juice')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('vinegar')
                ->setCorrect(true)
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('blood plasma')
        );
        $question->addAnswer(
            (new Answer())
                ->setAnswerText('lemon juice')
        );
        $manager->persist($question);

    }

    public function getDependencies(): array
    {
        return [
            SubjectFixtures::class,
        ];
    }
}
