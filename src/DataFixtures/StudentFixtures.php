<?php

namespace App\DataFixtures;

use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class StudentFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $student = (new Student())
            ->setName('Aether')
            ->setEmail('aether@example.com')
        ;
        $manager->persist($student);

        $student = (new Student())
            ->setName('Barbara')
            ->setEmail('barbara@example.com')
        ;
        $manager->persist($student);

        $student = (new Student())
            ->setName('Lisa')
            ->setEmail('lisa@example.com')
        ;
        $manager->persist($student);

        $manager->flush();
    }
}
