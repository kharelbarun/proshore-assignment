<?php

namespace App\DataFixtures;

use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SubjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $question = (new Subject())
            ->setName('Physics')
        ;
        $this->addReference('Subject/Physics', $question);
        $manager->persist($question);

        $question = (new Subject())
            ->setName('Chemistry')
        ;
        $this->addReference('Subject/Chemistry', $question);
        $manager->persist($question);

        $manager->flush();
    }
}
