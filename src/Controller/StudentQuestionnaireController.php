<?php

namespace App\Controller;

use App\Entity\StudentQuestionnaire;
use App\Entity\StudentQuestionnaireQuestionAnswer;
use App\Form\StudentQuestionnaireType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/student_questionnaire/{id}', name: 'student_questionnaire', methods: ['GET'])]
class StudentQuestionnaireController extends AbstractController
{
    public function __invoke(StudentQuestionnaire $studentQuestionnaire): Response
    {
        $studentQuestionnaire->getQuestionnaire()->getQuestionnaireQuestions()->toArray();
        if ($studentQuestionnaire->isSubmitted()) {
            return new Response('Already submitted');
        }

        foreach ($studentQuestionnaire->getQuestionnaire()->getQuestions() as $question) {
            $studentQuestionnaire->addStudentQuestionnaireQuestionAnswer(
                (new StudentQuestionnaireQuestionAnswer())
                    ->setQuestion($question)
            );
        }

        $form = $this->createForm(
            StudentQuestionnaireType::class,
            $studentQuestionnaire,
            [
                'method' => 'POST',
            ],
        );

        return $this->render('questionnaire_fill.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
