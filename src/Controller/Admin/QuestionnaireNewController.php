<?php

namespace App\Controller\Admin;

use App\Entity\Questionnaire;
use App\Form\QuestionnaireType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/questionnaire/new', name: 'questionnaire_new', methods: ['GET'])]
class QuestionnaireNewController extends AbstractController
{
	public function __invoke(): Response
    {
        $questionnaire = new Questionnaire();
        $form = $this->createForm(QuestionnaireType::class, $questionnaire, [
            'method' => 'POST',
        ]);

        return $this->render('admin/questionnaire_create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
