<?php

namespace App\Controller\Admin;

use App\Entity\Question;
use App\Entity\Questionnaire;
use App\Entity\QuestionnaireQuestion;
use App\Entity\Student;
use App\Entity\StudentQuestionnaire;
use App\Entity\Subject;
use App\Form\QuestionnaireType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

#[Route('/admin/questionnaire/new', name: 'questionnaire_new_save', methods: ['POST'])]
class QuestionnaireNewSaveController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private MailerInterface $mailer;

    private RouterInterface $router;

    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        RouterInterface $router,
        LoggerInterface $logger,
    ) {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->logger = $logger;
    }

    public function __invoke(Request $request): Response
    {
        $questionnaire = new Questionnaire();
        $form = $this->createForm(QuestionnaireType::class, $questionnaire, [
            'method' => 'POST',
        ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->addPhysicsQuestionsToQuestionnaire($questionnaire);
            $this->addChemistryQuestionsToQuestionnaire($questionnaire);

            $this->entityManager->persist($questionnaire);

            $studentQuestionnaires = $this->addStudentsToQuestionnaire($questionnaire);

            $this->entityManager->flush();

            foreach ($studentQuestionnaires as $studentQuestionnaire) {
                $questionnaireUrl = $this->router->generate('student_questionnaire', [
                    'id' => $studentQuestionnaire->getId(),
                ], RouterInterface::ABSOLUTE_URL);
                $email = (new Email())
                    ->from('admin@example.com')
                    ->to($studentQuestionnaire->getStudent()->getEmail())
                    ->subject('Questionnaire')
                    ->text('Fill the following questionnaire: '.$questionnaireUrl)
                ;
                $this->logger->info('email sent: '.$email->getTextBody());
                $this->mailer->send($email);
            }

            return $this->redirectToRoute('questionnaire_index');
        }

        return $this->render('admin/questionnaire_create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function addPhysicsQuestionsToQuestionnaire(Questionnaire $questionnaire): void
    {
        $questions = $this->getQuestionsBySubject($this->entityManager->find(Subject::class, 'Physics'));
        $this->addQuestionsToQuestionnaire($questionnaire, $questions);
    }

    private function addChemistryQuestionsToQuestionnaire(Questionnaire $questionnaire): void
    {
        $questions = $this->getQuestionsBySubject($this->entityManager->find(Subject::class, 'Chemistry'));
        $this->addQuestionsToQuestionnaire($questionnaire, $questions);
    }

    /**
     * @return StudentQuestionnaire[]
     */
    private function addStudentsToQuestionnaire(Questionnaire $questionnaire): array
    {
        $students = $this->getStudents();
        /** @var StudentQuestionnaire[] $studentQuestionnaires */
        $studentQuestionnaires = [];

        /** @var Student $student */
        foreach ($students as $student) {
            $studentQuestionnaire = (new StudentQuestionnaire())
                ->setQuestionnaire($questionnaire)
                ->setStudent($student)
            ;
            $this->entityManager->persist($studentQuestionnaire);
            $studentQuestionnaires[] = $studentQuestionnaire;
        }

        return $studentQuestionnaires;
    }

    /**
     * @return Question[]
     */
    private function getQuestionsBySubject($subject): array
    {
        $repo = $this->entityManager->getRepository(Question::class);
        return $repo->findBy([
            'subject' => $subject,
        ]);
    }

    /**
     * @return Question[]
     */
    private function getStudents(): array
    {
        $repo = $this->entityManager->getRepository(Student::class);
        return $repo->findAll();
    }

    /**
     * @param Question[] $questions
     */
    private function addQuestionsToQuestionnaire(Questionnaire $questionnaire, array $questions)
    {
        $shuffledQuestions = $questions;
        shuffle($shuffledQuestions);

        $shuffledQuestionsSubset = array_slice($shuffledQuestions, 0, 5);

        foreach ($shuffledQuestionsSubset as $question) {
            $questionnaireQuestion = new QuestionnaireQuestion();
            $questionnaireQuestion->setQuestionnaire($questionnaire);
            $questionnaireQuestion->setQuestion($question);
            $questionnaire->addQuestionnaireQuestion($questionnaireQuestion);
        }
    }
}
