<?php

namespace App\Controller\Admin;

use App\Entity\Questionnaire;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/questionnaire', name: 'questionnaire_index')]
class QuestionnaireIndexController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(): Response
    {
        $repo = $this->entityManager->getRepository(Questionnaire::class);
        $questionnaires = $repo->matching(
            (new Criteria())
                ->where(
                    Criteria::expr()->gt('expiryDate', new \DateTime())
                )
                ->orderBy(['id' => Criteria::DESC])
        );

        return $this->render('admin/questionnaire_index.html.twig', [
            'questionnaires' => $questionnaires,
        ]);
    }
}
