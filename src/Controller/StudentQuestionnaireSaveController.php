<?php

namespace App\Controller;

use App\Entity\StudentQuestionnaire;
use App\Entity\StudentQuestionnaireQuestionAnswer;
use App\Form\StudentQuestionnaireType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/student_questionnaire/{id}', name: 'student_questionnaire_save', methods: ['POST'])]
class StudentQuestionnaireSaveController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $request, StudentQuestionnaire $studentQuestionnaire): Response
    {
        $studentQuestionnaire->getQuestionnaire()->getQuestionnaireQuestions()->toArray();
        if ($studentQuestionnaire->isSubmitted()) {
            return new Response('Already submitted');
        }

        foreach ($studentQuestionnaire->getQuestionnaire()->getQuestions() as $question) {
            $studentQuestionnaire->addStudentQuestionnaireQuestionAnswer(
                (new StudentQuestionnaireQuestionAnswer())
                    ->setQuestion($question)
            );
        }

        $form = $this->createForm(
            StudentQuestionnaireType::class,
            $studentQuestionnaire,
            [
                'method' => 'POST',
            ],
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $studentQuestionnaire->setSubmitted(true);
            $this->entityManager->flush();
            return new Response('Thank you', 400);
        } else {
            return new Response('Bad request', 400);
        }
    }
}
