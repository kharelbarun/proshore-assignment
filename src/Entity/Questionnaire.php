<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Questionnaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $title;

    #[ORM\Column(type: 'date')]
    private \DateTime $expiryDate;

    #[ORM\OneToMany(targetEntity: QuestionnaireQuestion::class, mappedBy: 'questionnaire', cascade: ['persist'])]
    private Collection $questionnaireQuestions;

    public function __construct()
    {
        $this->questionnaireQuestions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getExpiryDate(): \DateTime
    {
        return $this->expiryDate;
    }

    public function setExpiryDate(\DateTime $expiryDate): self
    {
        $this->expiryDate = $expiryDate;
        return $this;
    }

    public function getQuestionnaireQuestions(): Collection
    {
        return $this->questionnaireQuestions;
    }

    public function addQuestionnaireQuestion(QuestionnaireQuestion $questionnaireQuestion): self
    {
        $this->questionnaireQuestions->add($questionnaireQuestion);
        return $this;
    }

    public function removeQuestionnaireQuestion(QuestionnaireQuestion $questionnaireQuestion): self
    {
        $this->questionnaireQuestions->removeElement($questionnaireQuestion);
        return $this;
    }

    public function getQuestions(): Collection
    {
        return $this->questionnaireQuestions->map(fn (QuestionnaireQuestion $qq) => $qq->getQuestion());
    }
}
