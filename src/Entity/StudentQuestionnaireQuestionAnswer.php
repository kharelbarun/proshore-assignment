<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class StudentQuestionnaireQuestionAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\ManyToOne]
    private Question $question;

    #[ORM\ManyToOne]
    private StudentQuestionnaire $studentQuestionnaire;

    #[ORM\ManyToOne]
    private Answer $answer;

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function setQuestion(Question $question): self
    {
        $this->question = $question;
        return $this;
    }

    public function getStudentQuestionnaire(): StudentQuestionnaire
    {
        return $this->studentQuestionnaire;
    }

    public function setStudentQuestionnaire(StudentQuestionnaire $studentQuestionnaire): self
    {
        $this->studentQuestionnaire = $studentQuestionnaire;
        return $this;
    }

    public function getAnswer(): Answer
    {
        return $this->answer;
    }

    public function setAnswer(Answer $answer): self
    {
        $this->answer = $answer;
        return $this;
    }
}
