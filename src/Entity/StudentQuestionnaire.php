<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class StudentQuestionnaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column]
    private bool $submitted = false;

    #[ORM\ManyToOne]
    private Student $student;

    #[ORM\ManyToOne]
    private Questionnaire $questionnaire;

    #[ORM\OneToMany(targetEntity: StudentQuestionnaireQuestionAnswer::class, mappedBy: 'studentQuestionnaire', cascade: ['persist'])]
    private Collection $studentQuestionnaireQuestionAnswers;

    public function __construct()
    {
        $this->studentQuestionnaireQuestionAnswers = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isSubmitted(): bool
    {
        return $this->submitted;
    }

    public function setSubmitted(bool $submitted): self
    {
        $this->submitted = $submitted;
        return $this;
    }

    public function getStudent(): Student
    {
        return $this->student;
    }

    public function setStudent(Student $student): self
    {
        $this->student = $student;
        return $this;
    }

    public function getQuestionnaire(): Questionnaire
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(Questionnaire $questionnaire): self
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    public function getStudentQuestionnaireQuestionAnswers(): Collection
    {
        return $this->studentQuestionnaireQuestionAnswers;
    }

    public function addStudentQuestionnaireQuestionAnswer(StudentQuestionnaireQuestionAnswer $sqqa): self
    {
        $this->studentQuestionnaireQuestionAnswers->add($sqqa);
        $sqqa->setStudentQuestionnaire($this);
        return $this;
    }

    public function removeStudentQuestionnaireQuestionAnswer(StudentQuestionnaireQuestionAnswer $sqqa): self
    {
        $this->studentQuestionnaireQuestionAnswers->remove($sqqa);
        return $this;
    }
}
