<?php

namespace App\Form;

use App\Entity\StudentQuestionnaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('studentQuestionnaireQuestionAnswers', CollectionType::class, [
                'entry_type' => StudentQuestionnaireQuestionType::class,
                'entry_options' => [
                    'parent_data' => $options['data'],
                    'label' => ' ',
                ],
                'label' => ' ',
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StudentQuestionnaire::class,
        ]);
    }
}
