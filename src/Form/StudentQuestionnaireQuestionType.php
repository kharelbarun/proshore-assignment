<?php

namespace App\Form;

use App\Entity\Answer;
use App\Entity\QuestionnaireQuestion;
use App\Entity\StudentQuestionnaire;
use App\Entity\StudentQuestionnaireQuestionAnswer;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentQuestionnaireQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var StudentQuestionnaire $studentQuestionnaire */
        $studentQuestionnaire = $options['parent_data'];
        $questionnaireQuestions = $studentQuestionnaire->getQuestionnaire()->getQuestionnaireQuestions();
        $index = $builder->getName();
        /** @var QuestionnaireQuestion $questionnaireQuestion */
        $questionnaireQuestion = $questionnaireQuestions[$index];
        $question = $questionnaireQuestion->getQuestion();

        $builder
            ->add('answer', EntityType::class, [
                'class' => Answer::class,
                'choice_label' => 'answerText',
                'query_builder' => function (EntityRepository $er) use ($question): QueryBuilder {
                    $qb = $er->createQueryBuilder('a');
                    $qb
                        ->where($qb->expr()->eq('a.question', ':question'))
                        ->setParameter(':question', $question)
                    ;
                    return $qb;
                },
                'placeholder' => '-- Choose an option --',
                'label' => ((int) $index + 1) . '. ' . $question->getQuestionText(),
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use (&$question) {
                /** @var StudentQuestionnaireQuestionAnswer $data */
                $data = $event->getData();
                $question = $data->getQuestion();
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StudentQuestionnaireQuestionAnswer::class,
        ]);
        $resolver->setRequired(['parent_data']);
    }
}
