# Instruction

## Setup

1. Run `composer install`
1. Run `./bin/console d:s:d -f`
1. Run `./bin/console d:s:c`
1. Run `./bin/console d:f:l -n`

## Create Questionnaire
1. Go to `{{PROJECT_BASE_URL}}/admin/questionnaire` in the browser. Replace `{{PROJECT_BASE_URL}}` with the base url of the project.
1. Click the "Create" link
1. Fill the form and submit

## Fill the form
1. Check invitation link in `/var/log/dev.log`. Search for log with label `app.INFO`. The invitation link has following path: `{{PROJECT_BASE_URL}}/student_questionnaire/{{ID}}`
1. Copy the invitation link and paste in the browser.
1. Fill the form and submit.
